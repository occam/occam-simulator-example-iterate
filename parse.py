import json

from occam import Occam

object = Occam.load()

f = open(object.output(), 'r')
result = int(f.read())
f.close()

o = open("output.json", "w+")
o.write(json.dumps({"result": result}))
o.close()
