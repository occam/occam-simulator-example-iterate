#include <stdio.h>

int main(int argc, char** argv) {
  int i = 0;
  int operation = 0;
  int start = 1;
  int end = 5;

  if (argc > 1) {
    operation = atoi(argv[1]);
  }

  if (argc > 2) {
    start = atoi(argv[2]);
  }

  if (argc > 3) {
    end = atoi(argv[3]);
  }

  int accumulator = start;
  for (i = start + 1; i <= end; i++) {
    if (operation == 0) {
      accumulator += i;
    }
    else {
      accumulator *= i;
    }
  }

  printf("%d\n", accumulator);

  return 0;
}
