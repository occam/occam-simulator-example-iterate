import os
from occam import Occam

object = Occam.load()

configuration = object.configuration("Options")

if configuration['operation'] == 'sum':
  argument = 0
else:
  argument = 1

scripts_path   = os.path.dirname(__file__)

print("hi")

command = "%s/iterator %s %s %s" % (scripts_path, argument, configuration['start'], configuration['end'])

finish_command = "python %s/parse.py" % (scripts_path)

Occam.report(command, finish_command)
